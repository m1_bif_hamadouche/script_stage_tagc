###################################################################################################
# Develloped by Toufik-H                                                                          #
# 05-02-2023                                                                                      #
#                                                                                                 #
# Script : Ce script permet un Web_scibing sur le eQTL catalogue de EBI et donc la récupération   #
#          des liens vers des fichiers contenat les données eQTL-gene des != cell immunitaire     #
#          ainis que une requette FTP pour récupéré ces eQTL pour chaque type cell dans un fichier#
#          au format tsv.                                                                         #
#                                                                                                 #
# ce script sera prochainement  automatisé comme suite                                            #
                            #en ligne de commande  :                                              #
                            # 1 Le script                                                         #
                            # 2 Liens générale du site eQTL EBI                                   #
                            # 3 une liste de cellules pour lesqulles en extrait des eQTL          #
###################################################################################################


### IMPORTATION PACKAGE
from selenium import webdriver
#from BeautifulSoup import BeautifulSoup
from bs4 import BeautifulSoup
import pandas as pd
import os
import urllib
from urllib.request import URLopener



# le driver pour lacer le hachage ds pages
#https://stackoverflow.com/questions/22476112/using-chromedriver-with-selenium-python-ubuntu
driver = webdriver.Chrome("/usr/bin/chromedriver")
# Lien vers la page a scrabé
driver.get("http://ftp.ebi.ac.uk/pub/databases/spot/eQTL/credible_sets/")


# récupération du contenue de la page
content = driver.page_source
soup = BeautifulSoup(content)


# récupération des liens d'interet
list_links= []
for a in soup.find_all('a', href=True):
    if '_ge_' in a['href'] or "CD19" in a['href'] or "CD14" in a['href'] or "CD15" in a['href'] : # <--- a optimiser !! + platelet
        list_links.append(a['href'])


#Récupéré les liens vers les données des céllules d'intéret
my_cells_list= ["macrophage","monocyte","LCL","NK","B-cell","T-cell","neutrophil","platelet","CD16","CD15","CD14","CD19","CD3"]

dict_cell_links = {} # dictionnaire des Cellules et leur liens en valeurs
for cell in my_cells_list:
    temp_list = []
    for link in list_links:
        if cell in link:
            temp_list.append(link)
            dict_cell_links[cell]=temp_list


# Pour récupéré les données gene-eQTL de chaque cellules
url_base='http://ftp.ebi.ac.uk/pub/databases/spot/eQTL/credible_sets/'
for key, val in dict_cell_links.items():
    print("Celle ",key)
    for link in range(len(val)):
            print("file N°",link," -- Link", val[link])

            path_file="/home/hm/Bureau/stage_M2_MArseille/eQTL_data_from_ebi/"+str(key)+"_cell_eQTL_data_fil-N°_"+str(link)+".tsv"
            if os.path.exists(path_file):
                pass
            else:
                # Lancer les requette avec les liens de chaque cellules
                urllib.request.urlcleanup()
                urllib.request.urlretrieve(url_base+val[link], path_file)