from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import LogisticRegression
import pandas as pd
import os

df = pd.read_csv("Data_for_regress_complete.tsv", sep="\t")
#y = df.iloc[:,429]
df = pd.concat([df.head(1000),df.tail(1000)])

x = df.iloc[:,1:429]
y = df.iloc[:,429]

scaler = StandardScaler()
x=scaler.fit_transform(x)

clf = LogisticRegression(max_iter = 1500,solver = "lbfgs",penalty = "l2").fit(x, y)
print("predict :",clf.predict(x))
print("score :",(clf.score(x, y)),"%")

dict_motifWeight= {}
for i in range(len(df.iloc[:,1:429].columns)):
    dict_motifWeight[df.iloc[:,1:429].columns[i]] = clf.coef_[0][i]
df_motif_weight=pd.DataFrame(dict_motifWeight.items(),columns=['Motifs','LogReg_Weights'])
print(df_motif_weight)
#print(df_motif_weight.loc[df_motif_weight['LogReg_Weights'] <0])

#https://stackoverflow.com/questions/57924484/finding-coefficients-for-logistic-regression-in-python
import statsmodels.api as sm
logit_model=sm.Logit(y,x)
result=logit_model.fit()
print(result.summary())
