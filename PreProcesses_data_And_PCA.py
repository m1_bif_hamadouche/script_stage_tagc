import numpy
import pandas as pd
import pandas
from sklearn import preprocessing
import seaborn as sns
import matplotlib.pyplot as plt
plt.style.use("ggplot")
from sklearn.decomposition import PCA

# Read first big data exported from docker ..  ---------------->>>
eqtl_data = pd.read_csv('/home/hm/Téléchargements/Selected_eQTL_DATA.csv',sep="\t")



# Select row with columns values containe this values : --> saved in files to be used after
# Data resume
#eqtl_data  = eqtl_data[eqtl_data.tissue_label.isin(["macrophage","LCL","NK cell","B cell" ])]
#print(eqtl_data.tissue_label.value_counts())

#eqtl_data.to_csv('eQTL_data_NK_Macro_LCL-B.tsv', sep='\t')



################################################################ WITH PIVOT--TABLE #####################################

#eqtl_data = pd.read_csv('eQTL_data_NK_Macro_LCL-B.tsv',sep="\t",index_col=False)
# Sinon directement sur le fichiers contenat toutes les cellules

"""
df_pivot_t = eqtl_data.pivot_table(
                                 values='z',
                                 index=['gene_id','variant_id'],
                                 columns="tissue_label",
                                 aggfunc="min")


df_pivot_t=df_pivot_t.fillna(0)
df_pivot_t.to_csv('pivot-T(funct=min)-all_celles.tsv', sep='\t')
"""

# Recharge le fichier de la table a pivoter_T
#df_pivot_t= pd.read_csv('pivot-T(funct=min)-all_celles.tsv',sep="\t")
#df_pivot_t = df_pivot_t.iloc[:,2:]


"""

            ###################### CORRILATION  ###################
corr_matrix = df_pivot_t.corr('pearson')
#corr_matrix = df_pivot_t.corr('spearman')
#print((corr_matrix *100))
sns.heatmap(df_pivot_t.corr('pearson'))
#sns.heatmap(df_pivot_t.corr('spearman'))
#plt.figure(figsize=(16, 6))
plt.title('Heatmap-corr-pear ; Data No-Normlz: z-score eQTL ; NO-filtred_by : pip ;pivot-Min; all celles')
plt.xlabel('celles-type')
plt.ylabel('celles-type')
plt.savefig("Heatmap-corr-pear-Zscore_eQTL-all-celles_No-Normalized-Val+pivot-Min_No-Pip.png")
plt.show()
"""


"""
###################### PCA  ###################
# changer l'order ds cllules/colonnes

cell_type  = ['B cell','LCL','CD16+ monocyte','monocyte','macrophage','neutrophil','CD4+ T cell', 'CD8+ T cell','T cell','Tfh cell', 'Th1 cell', 'Th17 cell', 'Th2 cell','Treg memory', 'Treg naive','NK cell']
df_pivot_t = df_pivot_t[cell_type]

# Prétraitement
cell_type = list(df_pivot_t.columns)

df_pivot_t = df_pivot_t.transpose()

X = df_pivot_t.values

# Normalisation/standardisation ---> ---> ---> ---> ---> ---> --->
#min_max_scaler = preprocessing.MinMaxScaler()
#X = min_max_scaler.fit_transform(X)


pca = PCA(n_components=2)
Xpc12_arr = pca.fit_transform(X)

#Nb de composante
nb = list(Xpc12_arr.shape)
print(nb)
colors =  ['red','firebrick','darkolivegreen','darkgreen','darkseagreen','forestgreen','cadetblue','aliceblue','blue',
           'cornflowerblue','lightblue','lightskyblue','royalblue','plum','pink','palevioletred']

sns.relplot(
    x=Xpc12_arr[:, 0],y=Xpc12_arr[:, 1],
    hue=cell_type,
    kind="scatter",
    facet_kws={"legend_out": True},
    hue_order=['B cell', 'LCL', 'CD16+ monocyte', 'monocyte', 'macrophage', 'neutrophil', 'CD4+ T cell', 'CD8+ T cell', 'T cell', 'Tfh cell', 'Th1 cell', 'Th17 cell', 'Th2 cell', 'Treg memory', 'Treg naive', 'NK cell'],
    palette=sns.color_palette(colors))


plt.title('PCA ; Data Normlz + pivot-min: z-score eQTL ; NO-filtred_by : pip')
plt.xlabel('PC1')
plt.ylabel('PC2')
plt.savefig("PCA-Zscore_eQTL-+ pivot-min-all_celles_No-Normalized-Val_No-Pip.png")
plt.show()

"""





# **********************************************************************************************************************





#ne fonctionne pas car l'indexe n'accepte pas des valeurs en doubles, qui dans notre cas des vaiantes en doubles ...
################################################################ WITH PIVOT  ###########################################

# Sinon directement sur le fichiers contenat toutes les cellules
path = "/home/hm/PycharmProjects/pythonProject_stage_M2_TAGC/script_stage_tagc2"

df_pivot_t = eqtl_data.pivot_table(
                                 values='z',
                                 index='variant_id',
                                 columns="tissue_label",
                                 aggfunc="mean")


#df_pivot_t=df_pivot_t.fillna(0)
#df_pivot_t.to_csv(path+'/pivot_T_index=variants(funct=mean)-all_celles.tsv', sep='\t')



# Recharge le fichier de la table a pivoter_T
df_pivot_t= pd.read_csv(path+'/pivot_T_index=variants(funct=mean)-all_celles.tsv',sep="\t")
df_pivot_t = df_pivot_t.iloc[:,1:]

            ###################### CORRILATION  ###################
corr_matrix = df_pivot_t.corr('pearson')
#corr_matrix = df_pivot_t.corr('spearman')
#print((corr_matrix *100))
sns.heatmap(df_pivot_t.corr('pearson'))
#sns.heatmap(df_pivot_t.corr('spearman'))
#plt.figure(figsize=(16, 6))
plt.title('Heatmap-corr-pear-indexe=variantes; Data No-Normlz: z-score eQTL ; NO-filtred_by:pip ;pivot-mean; all celles')
plt.xlabel('celles-type')
plt.ylabel('celles-type')
plt.savefig(path+"/Heatmap-corr-pears_indexe=variantes_Zscore_eQTL_all-celles_No-Normalized-Val_pivot-Mean_No-Pip.png")
plt.show()



###################### PCA  ###################
# changer l'order ds cllules/colonnes

cell_type  = ['B cell','LCL','CD16+ monocyte','monocyte','macrophage','neutrophil','CD4+ T cell', 'CD8+ T cell','T cell','Tfh cell', 'Th1 cell', 'Th17 cell', 'Th2 cell','Treg memory', 'Treg naive','NK cell']
df_pivot_t = df_pivot_t[cell_type]

# Prétraitement
cell_type = list(df_pivot_t.columns)

df_pivot_t = df_pivot_t.transpose()

X = df_pivot_t.values

# Normalisation/standardisation ---> ---> ---> ---> ---> ---> --->
#min_max_scaler = preprocessing.MinMaxScaler()
#X = min_max_scaler.fit_transform(X)


pca = PCA(n_components=2)
Xpc12_arr = pca.fit_transform(X)

#Nb de composante
nb = list(Xpc12_arr.shape)

colors =  ['red','firebrick','darkolivegreen','black','darkseagreen','forestgreen','cadetblue','aliceblue','blue',
           'cornflowerblue','lightblue','lightskyblue','royalblue','plum','pink','palevioletred']

sns.relplot(
    x=Xpc12_arr[:, 0],y=Xpc12_arr[:, 1],
    hue=cell_type,
    kind="scatter",
    facet_kws={"legend_out": True},
    hue_order=['B cell', 'LCL', 'CD16+ monocyte', 'monocyte', 'macrophage', 'neutrophil', 'CD4+ T cell', 'CD8+ T cell', 'T cell', 'Tfh cell', 'Th1 cell', 'Th17 cell', 'Th2 cell', 'Treg memory', 'Treg naive', 'NK cell'],
    palette=sns.color_palette(colors))


plt.title('PCA ; Data Normlz +_indexe=variants_pivot-mean: z-score eQTL ; NO-filtred_by : pip')
plt.xlabel('PC1')
plt.ylabel('PC2')
plt.savefig(path+"/PCA_Zscore-eQTL_+pivot-mean_indexe=variants_all_celles_No-Normalized-Val_No-Pip.png")
plt.show()



