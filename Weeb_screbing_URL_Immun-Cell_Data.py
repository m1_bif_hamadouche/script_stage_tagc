import pandas as pd
import re
from pandas import *
#Lien vers site/gitHub pour récupéré les liens
eqtl_url = "https://raw.githubusercontent.com/eQTL-Catalogue/eQTL-Catalogue-resources/master/tabix/tabix_ftp_paths.tsv"

#Construction data frame pour stocké les liens ! --> Weeb Screebing :
eqtl_df = pd.read_csv(eqtl_url, sep="\t", header=0)

# Trié sur les liens contenat que les eQTL des les génes :
eqtl_df = eqtl_df.loc[eqtl_df['ftp_path'].str.contains('/ge/|/microarray/',regex=True,na=False), ]
# récupére la colonne des liens qui nous intéresse
eqtl_df=eqtl_df.iloc[:,8]



#Filtré les liens des céllules d'intéret
my_cells_list= ["macrophage","monocyte","LCL","NK","B-cell","T-cell","neutrophil","platelet","CD16","CD15","CD14","CD19","CD3"]
    #Granulocyte non prise == abscence en DB
    # Construire dictionnaire --> keys = cells , values = list of links
dic_Cell_Link = {}
for cell in my_cells_list:
    #print("Cells-type : ",cell,"\nLink: \n", eqtl_df[eqtl_df.iloc[:,].str.contains(cell)],"\n\n")
    dic_Cell_Link[cell] = list(eqtl_df[eqtl_df.iloc[:,].str.contains(cell)])

# Pour affiché les Cellules + liens
for key, val in dic_Cell_Link.items():
    print(key,"\n",len(val))
    print(key,"\n",val)



