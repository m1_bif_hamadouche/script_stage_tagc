import shutil
from urllib.request import URLopener
opener = URLopener()

# URL
url = 'ftp://ftp.ebi.ac.uk/pub/databases/spot/eQTL/sumstats/BLUEPRINT/ge/BLUEPRINT_SE_ge_monocyte.all.tsv.gz'
# Storage (disque)
store_path = '/home/hm/Bureau/stage_M2_MArseille/eQTL_data_from_ebi/test_file.tsv'


with opener.open(url) as remote_file, open(store_path, 'wb') as local_file:
    shutil.copyfileobj(remote_file, local_file)