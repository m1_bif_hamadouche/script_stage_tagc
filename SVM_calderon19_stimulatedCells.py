##########################################
#
""" Programme automatiser                # 
    entré les varibales                  #
    souhaiter en ligne de                #
        commande"""  #
###########################################

import numpy as np
np.random.seed(6) # 2 = 62.12 # 3= 62.23#4= 62.32 #5=62.33999 #6=63.36, #7=58, # 8=61, #9 =61.95
import pandas as pd
from sklearn.linear_model import SGDClassifier
from sklearn import metrics
from sklearn import svm
from numpy.random import seed
from sklearn.naive_bayes import GaussianNB
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import confusion_matrix
from sklearn.svm import LinearSVC
from sklearn.ensemble import BaggingClassifier
#import hasy_tools  # pip install hasy_tools
import os
import sys

# Récupération des variables en ligne de commande (Terminal)
#model_tester = np.r_[np.array(sys.argv[1:]).astype(np.int64)]

df = pd.read_csv("Data_for_regress_complete.tsv", sep="\t")
# Création du fichier Train ( pour entrainer le modél svm)
#y = df.iloc[:,429]
#X = df.iloc[0:500, 1:429]

X1 = df.head(1000)
X2 = df.tail(1000)
df = pd.concat([X1,X2])

#dft = df.iloc[5000:15000,:]

with open("ResultatsBagging+SGD" + str("all_variables") + ".txt", "w") as f1, open(
        "ResultatsNet" + str("all_variables") + ".txt", "w") as f2:
    for j in range(1, 2):

        for i in range(1, 2):
            iteration = 0
            iteration =iteration +i
            print("************************", i)
            print("\n_________________________", iteration, file=f1)
            print("\n_________________________", iteration, file=f2)

            # Charger les varibales de l'ensemble Train
            # fileName = "sampleTrain.xlsx"
            # df = pd.read_excel(fileName, index_col=0)

            # Transformer les healthe value en catégories
            region_stat = df.PC_1.astype('category')

            # Les vrais health values de chaque individus
            y = (region_stat.cat.codes)

            # les variables utiliser dans le modéle:
            X = df.iloc[:, 1:429]
            #scaler = StandardScaler()
            #X_scaled = scaler.fit_transform(X)

            ##############  Les classifieurs utiliser :  vous pouvez a tout moment changer de classificateur #############
            # ** 3 SGD :
            svm = SGDClassifier(penalty='l1',learning_rate='optimal',random_state=6)
            clf = BaggingClassifier(estimator=svm)

            # Fonction d'apprentissage supervisé
            clf.fit(X, y)

            # Prédiction sur les des données Train : \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
            print("\nLe modéle utilser : ", clf, "\n", file=f1)  #########
            print("\nLe modéle utilser : ", clf, "\n", file=f2)  #########
            print("Modele_variable : ", "All_variables", "\n", 'All _variables', "\n", file=f2)

            # Les health statuts des volentaires
            #vrais_statut_Train = (region_stat.cat.codes)
            statut_actu_Train = []
            for i in region_stat.cat.codes:
                statut_actu_Train.append(i)

            # Phase de prédiction sur les données Train
            #prediction1 = clf.predict(X)
            # Récupération des statuts prédites:
            statuts_predit_train = []
            for i in clf.predict(X):
                statuts_predit_train.append(i)

            #Déduction des résulats de classemet Train :matrice de confusion
            result_predict_train = confusion_matrix(statut_actu_Train, statuts_predit_train)


            print("Modele_variable : ", "All variables", "\n", "All variables", "\n", file=f1)  ##########
            print("- Train :", file=f1)
            print("Matrice confusion Train :\n", result_predict_train, file=f1)  ##########
            print("- Train :", file=f2)
            # Déduction des % de résulats juste sur Train :
            print("% des résulats justes  : ", (metrics.accuracy_score(statut_actu_Train, statuts_predit_train)) * 100,
                  file=f1)  ########
            print("% des résulats justes  : ", (metrics.accuracy_score(statut_actu_Train, statuts_predit_train)) * 100,
                  file=f2)  ########

f1.close()
f2.close()