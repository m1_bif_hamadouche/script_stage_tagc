# https://rukshanpramoditha.medium.com/principal-component-analysis-pca-with-scikit-learn-1e84a0c731b0
import numpy as np
import numpy as numpy
import pandas
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
plt.style.use("ggplot")
from sklearn.decomposition import PCA

df = pandas.read_csv("matrix_for-PCA.txt", sep='\t')

cell_type = list(df.columns)
df = df.transpose()

X = df.values

pca = PCA(n_components=2)
Xpc12_arr = pca.fit_transform(X)

#Nb de composante
nb = list(Xpc12_arr.shape)


colors = ["steelblue", "deepskyblue", "skyblue", "lightskyblue", "darkred", "firebrick", "maroon", "sienna",
          "indianred", "red", "orangered", "coral", "salmon", "lightsalmon", "darkorange","gray","tomato",
          "Orange","DodgerBlue","MediumSeaGreen","SlateBlue","SlateBlue","Violet","LightGray","purple","cyan",
          "olive","pink","brown","black","yellow","magenta","darkgreen" ]#https://www.webucator.com/article/python-color-constants-module/


sns.relplot(
    x=Xpc12_arr[:, 0],y=Xpc12_arr[:, 1],
    hue=cell_type,
    kind="scatter",
    facet_kws={"legend_out": True},
    hue_order=['monocyte_LPS', 'CD8_T-cell_naive', 'microarray_T-cell_CD4', 'T-cell', 'CD4_T-cell_naive','Fairfax_2014_microarray_monocyte_IFN24', 'Treg_naive', 'macrophage_IFNg+Salmonella', 'Th2_memory', 'macrophage_Listeria', 'monocyte_IAV', 'macrophage_Salmonella', 'B-cell_CD19', 'Tfh_memory', 'B-cell_naive','Th17_memory', 'Th1-17_memory', 'macrophage_naive', 'Fairfax_2014_microarray_monocyte_LPS24', 'monocyte_CD14', 'Treg_memory', 'CD4_T-cell_anti-CD3-CD28', 'T-cell_CD8', 'BLUEPRINT_SE_ge_monocyte', 'Fairfax_2014_microarray_monocyte_naive', 'microarray_T-cell_CD8', 'Fairfax_2014_microarray_monocyte_LPS2', 'monocyte_CD16_naive', 'CD8_T-cell_anti-CD3-CD28', 'monocyte_naive', 'monocyte_Pam3CSK4', 'macrophage_IFNg', 'NK-cell_naive'],
    palette=sns.color_palette(colors))


plt.title('PCA')
plt.xlabel('PC1')
plt.ylabel('PC2')
plt.show()




